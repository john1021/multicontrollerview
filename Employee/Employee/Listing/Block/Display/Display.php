<?php
/**
 * Copyright © 2015 Employee . All rights reserved.
 */
namespace Employee\Listing\Block\Display;
class Display extends \Magento\Framework\View\Element\Template
{
	protected $_modelContactFactory;
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Employee\Listing\Model\ContactFactory $modelContactFactory
	)
	{
		$this->_modelContactFactory = $modelContactFactory;
		parent::__construct($context);
	}

	public function sayHello()
	{
		return __('Hello World');
	}

	public function getConatctCollection(){
	    $ContactModel = $this->_modelContactFactory->create();
        return $ContactModel->getCollection();
	}
}
